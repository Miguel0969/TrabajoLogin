<%-- 
    Document   : index
    Created on : 28-ago-2018, 19:28:47
    Author     : PtoMontt_B305
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Java web</title>
        <link href="css/css.css" 
              >
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
    </head>
    <body id="indexbd">
        <div class="container" style="margin-top:50px;" >
            <div class="row">
                <div class="col-md-4 col-md-offset-4 text-center">
                    <h1 id="pl">Bienvenido al gestor de usuarios</h1>
                    <hr>
                    <p id="pl">Esta pagina sirve para gestionar los usuarios que hay dento de la base de datos</p>
                    <p id="pl">Para poder acceder a esta pagina debe tener registrado un usuario</p>

                    <a class="btn btn-primary" role="button" href="Login.jsp" style="margin-top: 50px;">Iniciar Sesion</a> 
                    <p id="pl">Presione iniciar secion para cargar el login</p>
                </div>
            </div>
        </div>
    </body>
</html>
